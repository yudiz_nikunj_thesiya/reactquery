import React from "react";
import PropTypes from "prop-types";

const PostCard = ({ title, desc, tags, reactions }) => {
	return (
		<div className="px-10 bg-indigo-800 rounded-xl text-indigo-400 flex flex-col space-y-3 py-8 justify-between">
			<div className="flex flex-col space-y-3">
				<h2 className="text-xl text-indigo-200 font-extrabold">{title}</h2>
				<p>{desc}</p>
			</div>

			<div className=" text-indigo-200 flex flex-col lg:flex-row items-start lg:items-center justify-between gap-4">
				<span className="px-4 w-full text-center uppercase py-3 text-indigo-200 rounded-lg border border-indigo-700 text-xs md:text-sm bg-indigo-900 whitespace-nowrap">
					REACTIONS {reactions}
				</span>
				<div className="flex lg:flex-row w-full flex-col md:items-center lg:space-x-3 space-y-2 lg:space-y-0">
					{tags.map((tag, index) => (
						<span
							className="px-4 w-full uppercase text-center py-3 text-indigo-200 rounded-lg border border-indigo-700 text-xs md:text-sm bg-indigo-900"
							key={index}
						>
							{tag}
						</span>
					))}
				</div>
			</div>
		</div>
	);
};

export default PostCard;

PostCard.propTypes = {
	title: PropTypes.string,
	desc: PropTypes.string,
	tags: PropTypes.array,
	reactions: PropTypes.number,
};
