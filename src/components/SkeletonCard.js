import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const SkeletonCard = () => {
	return (
		<SkeletonTheme baseColor="#302E80" highlightColor="#4f46e5">
			<div className="px-10 bg-indigo-800 rounded-xl text-indigo-400 flex flex-col space-y-3 py-8 justify-between">
				<div className="flex flex-col space-y-3">
					<Skeleton
						width="90%"
						height={28}
						className="text-xl bg-indigo-800 font-extrabold"
					></Skeleton>
					<Skeleton count={5}></Skeleton>
				</div>
				<div className=" text-indigo-200 flex flex-col md:flex-row items-start md:items-center justify-between gap-4">
					<Skeleton className="px-10 uppercase py-3 text-indigo-200 rounded-lg text-xs md:text-sm bg-indigo-900"></Skeleton>
					<div className="flex items-center space-x-3">
						<Skeleton className="px-8 uppercase py-3 text-indigo-800 rounded-lg text-sm bg-indigo-900"></Skeleton>
						<Skeleton className="px-8 uppercase py-3 text-indigo-800 rounded-lg text-sm bg-indigo-900"></Skeleton>
						<Skeleton className="px-8 uppercase py-3 text-indigo-800 rounded-lg text-sm bg-indigo-900"></Skeleton>
					</div>
				</div>
			</div>
		</SkeletonTheme>
	);
};

export default SkeletonCard;
