import React from "react";
import { NavLink } from "react-router-dom";

const Nav = () => {
	return (
		<div className="bg-indigo-900 sticky top-0 w-full box-border flex items-center justify-between px-10 py-6 z-50">
			<div className="text-indigo-100 text-2xl font-bold">ReactQuery</div>
			<div className="md:flex hidden items-center space-x-3 text-indigo-300">
				<NavLink
					to="/"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					Home
				</NavLink>
				<NavLink
					to="/users"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					Users
				</NavLink>
			</div>
			<button className="text-base text-indigo-900 bg-indigo-100 px-8 py-4 rounded-full">
				Register
			</button>
		</div>
	);
};

export default Nav;
