import React from "react";
import Nav from "../components/Nav";

const Users = () => {
	return (
		<div className="bg-indigo-900 text-indigo-100 min-h-screen flex flex-col items-center">
			<Nav />
			<span className="text-2xl">Users</span>
		</div>
	);
};

export default Users;
