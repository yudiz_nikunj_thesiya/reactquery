import React from "react";
import { useQuery } from "react-query";
import Nav from "../components/Nav";
import PostCard from "../components/PostCard";
import SkeletonCard from "../components/SkeletonCard";

const fetchProducts = async () => {
	const res = await fetch("https://dummyjson.com/posts");
	return res.json();
};

const Home = () => {
	const { data, isLoading, isError, error, isSuccess, isFetching } = useQuery(
		"posts",
		fetchProducts,
		{
			staleTime: 20000,
		}
	);

	return (
		<div className="bg-indigo-900 text-indigo-100 min-h-screen">
			<Nav />
			{isLoading && (
				<div className="grid pb-10 grid-cols-1 md:grid-cols-2 px-12 gap-4">
					<SkeletonCard />
					<SkeletonCard />
					<SkeletonCard />
					<SkeletonCard />
					<SkeletonCard />
					<SkeletonCard />
				</div>
			)}
			{isError && <div className="px-12">{error.message}</div>}
			{isSuccess && (
				<div className="bg-indigo-900 px-12 pb-12 grid grid-cols-1 md:grid-cols-2 gap-4">
					{data.posts.map((post, index) =>
						isFetching ? (
							<SkeletonCard key={index} />
						) : (
							<PostCard
								title={post.title}
								desc={post.body}
								tags={post.tags}
								reactions={post.reactions}
								key={post.id}
								id={post.id}
							/>
						)
					)}
				</div>
			)}
		</div>
	);
};

export default Home;
