import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { Route, Routes, useLocation } from "react-router-dom";
import Home from "./pages/Home";
import ReactGA from "react-ga";
import { ReactQueryDevtools } from "react-query/devtools";
import Users from "./pages/Users";
import { useEffect } from "react";
const queryClient = new QueryClient();

const TRACKING_ID = "UA-226047133-1";
ReactGA.initialize(TRACKING_ID);

function App() {
	const location = useLocation();
	useEffect(() => {
		ReactGA.pageview(window.location.pathname + window.location.search);
	}, [location.pathname]);

	const methodDoesNotExist = () => {
		const a = {};
		a.push(10);
	};

	return (
		<div className="App bg-indigo-900">
			<QueryClientProvider client={queryClient}>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/users" element={<Users />} />
				</Routes>
				<button className="btn m-10" onClick={methodDoesNotExist}>
					Sentry
				</button>
				<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
			</QueryClientProvider>
		</div>
	);
}

export default App;
